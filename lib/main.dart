import 'package:flutter/material.dart';
import 'package:login_demo/app/models/theme.dart';
import 'package:login_demo/app/models/user.dart';
import 'package:provider/provider.dart';
import 'app/app.dart';

void main() {
  final app = MultiProvider(
    providers: [
      ChangeNotifierProvider<AppTheme>(create: (context) => AppTheme()),
      ChangeNotifierProvider<AppUser>(create: (context) => AppUser()),
    ],
    child: const LoginDemoApp(),
  );

  runApp(app);
}
