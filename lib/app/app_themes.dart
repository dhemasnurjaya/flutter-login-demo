import 'package:flutter/material.dart';

class AppThemes {
  late ThemeData _baseTheme;

  AppThemes(ThemeData baseThemeData) {
    _baseTheme = baseThemeData.copyWith(
      useMaterial3: true,
      cardTheme: const CardTheme(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
        margin: EdgeInsets.symmetric(
          horizontal: 16.0,
        ),
      ),
      buttonTheme: const ButtonThemeData(
        height: 48.0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
      ),
    );
  }

  ThemeData build() {
    if (_baseTheme.brightness == Brightness.dark) {
      return _buildDarkTheme();
    }

    return _buildLightTheme();
  }

  ThemeData _buildDarkTheme() {
    const primaryColor = Color(0xFF131726);
    final themeData = _baseTheme.copyWith(
      colorScheme: ColorScheme.fromSeed(
        seedColor: primaryColor,
        brightness: Brightness.dark,
      ),
      inputDecorationTheme: _baseTheme.inputDecorationTheme.copyWith(
        enabledBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white54),
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: Colors.white),
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
      ),
      textTheme: _baseTheme.textTheme.copyWith(
        headlineMedium: _baseTheme.textTheme.headlineMedium?.copyWith(
          color: Colors.white,
        ),
        titleMedium: _baseTheme.textTheme.titleMedium?.copyWith(
          color: Colors.white54,
        ),
        bodyMedium: _baseTheme.textTheme.bodyMedium?.copyWith(
          color: Colors.white38,
        ),
      ),
    );

    return themeData;
  }

  ThemeData _buildLightTheme() {
    const primaryColor = Color(0xFF1E88E5);
    var themeData = _baseTheme.copyWith(
      colorScheme: ColorScheme.fromSeed(
        seedColor: primaryColor,
        brightness: Brightness.light,
      ),
      inputDecorationTheme: _baseTheme.inputDecorationTheme.copyWith(
        enabledBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: Colors.black12),
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
        focusedBorder: const OutlineInputBorder(
          borderSide: BorderSide(color: primaryColor),
          borderRadius: BorderRadius.all(Radius.circular(8.0)),
        ),
      ),
      textTheme: _baseTheme.textTheme.copyWith(
        headlineMedium: _baseTheme.textTheme.headlineMedium?.copyWith(
          color: Colors.black,
        ),
        titleMedium: _baseTheme.textTheme.titleMedium?.copyWith(
          color: Colors.black54,
        ),
        bodyMedium: _baseTheme.textTheme.bodyMedium?.copyWith(
          color: Colors.black38,
        ),
      ),
    );

    return themeData;
  }
}
