import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:login_demo/app/models/theme.dart';
import 'package:provider/provider.dart';

class AppBaseWidget extends StatelessWidget {
  final List<Widget> children;

  const AppBaseWidget({required this.children, super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.background,
      body: Container(
        height: MediaQuery.of(context).size.height,
        width: MediaQuery.of(context).size.width,
        decoration: const BoxDecoration(
          image: DecorationImage(
            image: AssetImage("assets/background.png"),
            fit: BoxFit.cover,
          ),
        ),
        child: Stack(
          children: [
            Positioned(
              top: MediaQuery.of(context).viewPadding.top + 16.0,
              right: 16.0,
              child: InkWell(
                onTap: () => context.read<AppTheme>().toggleTheme(),
                child: SvgPicture.asset(
                  context.read<AppTheme>().getThemeMode() == ThemeMode.dark
                      ? "assets/ic_sun.svg"
                      : "assets/ic_moon.svg",
                  fit: BoxFit.cover,
                  height: 32.0,
                ),
              ),
            ),
            ...children,
          ],
        ),
      ),
    );
  }
}
