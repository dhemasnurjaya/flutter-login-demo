import 'package:flutter/material.dart';
import 'package:login_demo/app/app_themes.dart';
import 'package:login_demo/app/models/theme.dart';
import 'package:login_demo/app/pages/login_page.dart';
import 'package:provider/provider.dart';

class LoginDemoApp extends StatelessWidget {
  const LoginDemoApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Login Demo',
      theme: AppThemes(ThemeData.light()).build(),
      darkTheme: AppThemes(ThemeData.dark()).build(),
      themeMode: context.watch<AppTheme>().getThemeMode(),
      home: const LoginPage(),
    );
  }
}
