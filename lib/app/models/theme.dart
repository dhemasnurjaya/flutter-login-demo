import 'package:flutter/material.dart';

class AppTheme with ChangeNotifier {
  late ThemeMode _themeMode;

  ThemeMode getThemeMode() => _themeMode;

  AppTheme() {
    _themeMode = ThemeMode.light;
  }

  void toggleTheme() {
    _themeMode =
        _themeMode == ThemeMode.dark ? ThemeMode.light : ThemeMode.dark;
    notifyListeners();
  }
}
