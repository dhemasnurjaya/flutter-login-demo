import 'package:flutter/material.dart';

class AppUser with ChangeNotifier {
  String? _loggedUserEmail;

  String? getLoggedUserEmail() => _loggedUserEmail;

  void saveUser(String? userEmail) {
    _loggedUserEmail = userEmail;
    notifyListeners();
  }
}
