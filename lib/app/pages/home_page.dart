import 'package:flutter/material.dart';
import 'package:login_demo/app/models/user.dart';
import 'package:login_demo/app/pages/login_page.dart';
import 'package:login_demo/app/strings_en.dart';
import 'package:login_demo/app/widgets/app_base.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  const HomePage({super.key});

  void _onLogOutButtonPressed(BuildContext context) {
    context.read<AppUser>().saveUser(null);
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (_) => const LoginPage(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    var homeCard = Card(
      child: Padding(
        padding: const EdgeInsets.all(36.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              AppStrings.homePageTitle,
              style: Theme.of(context).textTheme.headlineMedium?.copyWith(
                    fontWeight: FontWeight.w600,
                  ),
            ),
            const SizedBox(height: 16.0),
            Text(
              AppStrings.homePageDesc(
                  context.watch<AppUser>().getLoggedUserEmail() ?? ""),
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(height: 32.0),
            MaterialButton(
              onPressed: () => _onLogOutButtonPressed(context),
              color: const Color(0xFF1E88E5),
              child: Text(
                AppStrings.logOutButton,
                style: Theme.of(context).textTheme.bodyLarge?.copyWith(
                      fontSize: 16.0,
                      color: Colors.white,
                    ),
              ),
            ),
          ],
        ),
      ),
    );

    return AppBaseWidget(
      children: [
        Align(
          alignment: Alignment.center,
          child: SingleChildScrollView(
            child: homeCard,
          ),
        ),
      ],
    );
  }
}
