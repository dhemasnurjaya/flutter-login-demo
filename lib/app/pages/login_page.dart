import 'package:flutter/material.dart';
import 'package:login_demo/app/models/user.dart';
import 'package:login_demo/app/pages/home_page.dart';
import 'package:login_demo/app/strings_en.dart';
import 'package:login_demo/app/widgets/app_base.dart';
import 'package:provider/provider.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({super.key});

  @override
  State<LoginPage> createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  final _emailFieldController = TextEditingController();
  final _passwordFieldController = TextEditingController();
  var _isPasswordVisible = false;
  var _isLogInButtonEnabled = false;

  @override
  void initState() {
    super.initState();
  }

  void _onTogglePasswordPressed() {
    setState(() => _isPasswordVisible = !_isPasswordVisible);
  }

  void _onTextFieldChanged(String value) {
    final email = _emailFieldController.text;
    final password = _passwordFieldController.text;

    setState(
        () => _isLogInButtonEnabled = email.isNotEmpty && password.isNotEmpty);
  }

  void _onLogInButtonPressed(BuildContext context) {
    context.read<AppUser>().saveUser(_emailFieldController.text);
    Navigator.of(context).pushReplacement(
      MaterialPageRoute(
        builder: (_) => const HomePage(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    final loginCard = Card(
      child: Padding(
        padding: const EdgeInsets.all(36.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: [
            Text(
              AppStrings.loginPageTitle,
              style: Theme.of(context).textTheme.headlineMedium?.copyWith(
                    fontWeight: FontWeight.w600,
                  ),
            ),
            const SizedBox(height: 16.0),
            Text(
              AppStrings.loginPageDesc,
              style: Theme.of(context).textTheme.titleMedium,
            ),
            const SizedBox(height: 16.0),
            Text(
              AppStrings.emailFieldTitle,
              style: Theme.of(context).textTheme.labelLarge?.copyWith(
                    fontSize: 16.0,
                  ),
            ),
            const SizedBox(height: 8.0),
            TextField(
              key: const Key("logInEmailField"),
              controller: _emailFieldController,
              onChanged: _onTextFieldChanged,
              keyboardType: TextInputType.emailAddress,
              textInputAction: TextInputAction.next,
              decoration: InputDecoration(
                floatingLabelBehavior: FloatingLabelBehavior.never,
                contentPadding: const EdgeInsets.all(12.0),
                hintText: AppStrings.emailFieldHint,
                hintStyle: Theme.of(context).textTheme.bodyMedium?.copyWith(
                      fontSize: 16.0,
                    ),
              ),
            ),
            const SizedBox(height: 16.0),
            Text(
              AppStrings.passwordFieldTitle,
              style: Theme.of(context).textTheme.labelLarge?.copyWith(
                    fontSize: 16.0,
                  ),
            ),
            const SizedBox(height: 8.0),
            TextField(
              key: const Key("logInPasswordField"),
              controller: _passwordFieldController,
              onChanged: _onTextFieldChanged,
              keyboardType: TextInputType.visiblePassword,
              textInputAction: TextInputAction.done,
              obscureText: !_isPasswordVisible,
              decoration: InputDecoration(
                floatingLabelBehavior: FloatingLabelBehavior.never,
                contentPadding: const EdgeInsets.all(12.0),
                hintText: AppStrings.passwordFieldHint,
                hintStyle: Theme.of(context).textTheme.bodyMedium?.copyWith(
                      fontSize: 16.0,
                    ),
                suffixIcon: IconButton(
                  onPressed: _onTogglePasswordPressed,
                  icon: Icon(
                    _isPasswordVisible
                        ? Icons.visibility_off
                        : Icons.visibility,
                  ),
                ),
              ),
            ),
            const SizedBox(height: 32.0),
            MaterialButton(
              key: const Key("logInButton"),
              onPressed: _isLogInButtonEnabled
                  ? () => _onLogInButtonPressed(context)
                  : null,
              color: const Color(0xFF1E88E5),
              disabledColor: Colors.blueGrey,
              child: Text(
                AppStrings.logInButton,
                style: Theme.of(context).textTheme.bodyLarge?.copyWith(
                      fontSize: 16.0,
                      color: Colors.white,
                    ),
              ),
            ),
          ],
        ),
      ),
    );

    return AppBaseWidget(
      children: [
        Align(
          alignment: Alignment.center,
          child: SingleChildScrollView(
            child: loginCard,
          ),
        ),
      ],
    );
  }
}
