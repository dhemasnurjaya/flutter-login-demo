abstract class AppStrings {
  static const loginPageTitle = "Let's go!";
  static const loginPageDesc =
      "Login into your account. You will use this email and password to log into your accounts for all access.";
  static const homePageTitle = "Welcome back!";
  static String homePageDesc(String userEmail) =>
      "You are logged in as $userEmail. Not you? Log out now!";

  static const emailFieldTitle = "Email";
  static const emailFieldHint = "Enter your email";
  static const passwordFieldTitle = "Password";
  static const passwordFieldHint = "Password";
  static const logInButton = "Log In";
  static const logOutButton = "Log Out";
}
