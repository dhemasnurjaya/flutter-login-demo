# login app demo

Login App demo built by Flutter for Neutrino developer skill test, [Figma reference](https://www.figma.com/file/mR9qkURTeTXzG2HH3jkIyj/Test-Project---Dhemas?type=design&mode=design&t=EmsxUu2Wv9bEzXEA-1).

## How I built the app

I wrote the app using minimal dependencies:
- [flutter_svg](https://pub.dev/packages/flutter_svg) for svg assets support.
- [provider](https://pub.dev/packages/provider) for managing global app states. I use this package because it is recommended in the [official simple state management example](https://docs.flutter.dev/data-and-backend/state-mgmt/simple).

Dev note:
- Resource strings separated in a class just for the demo, for proper development better use [intl](https://pub.dev/packages/intl) to store string res and provide language translations.
- User email is saved as a global app state that is not mandatory for this demo because the app doesn't persist the state when the app closed, I can just put it in the HomePage constructor. But I think it is a proper way to do it in a real app to manage user session.

## What can be improved
- Dark mode toggle button will be covered under the log in form when the virtual keyboard appeared (need to confirm with UX dev for better behavior).
- Add complete input validation in login screen using [TextFormField's validator](https://api.flutter.dev/flutter/material/TextFormField-class.html) (valid email/password format, length, etc), and use error message for each text field instead of disabling the log in button.
- More accurate color pallete, for this demo I only using Flutter's built-in color generator using base color in the Figma refs as color seed.
