import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:login_demo/app/app.dart';
import 'package:login_demo/app/models/theme.dart';
import 'package:login_demo/app/models/user.dart';
import 'package:provider/provider.dart';

void testLogInButtonEnabled(WidgetTester tester, dynamic matcher) {
  expect(
      tester
          .widget<MaterialButton>(find.byKey(const Key("logInButton")))
          .enabled,
      matcher);
}

void main() {
  testWidgets('Login Page smoke test', (WidgetTester tester) async {
    await tester.pumpWidget(
      MultiProvider(
        providers: [
          ChangeNotifierProvider<AppTheme>(create: (context) => AppTheme()),
          ChangeNotifierProvider<AppUser>(create: (context) => AppUser()),
        ],
        child: const LoginDemoApp(),
      ),
    );

    final emailField = find.byKey(const Key("logInEmailField"));
    final passwordField = find.byKey(const Key("logInPasswordField"));

    // login button should disabled on first launch
    testLogInButtonEnabled(tester, isFalse);

    // login button should disabled when only email field filled
    await tester.enterText(emailField, "example@email.com");
    testLogInButtonEnabled(tester, isFalse);

    // login button should disabled when only email field filled
    await tester.enterText(emailField, "");
    await tester.enterText(passwordField, "p4ssw0rD");
    testLogInButtonEnabled(tester, isFalse);

    // login button should enabled when all field filled
    await tester.enterText(emailField, "example@email.com");
    await tester.enterText(passwordField, "p4ssw0rD");
    testLogInButtonEnabled(tester, isTrue);
  });
}
